<?php

namespace App\Service;

use App\Exception\InvalidDateValueException;
use App\Exception\InvalidTimezoneException;

class DateTimezoneService
{
    /**
     * @param \DateTimeInterface $dateTime
     * @param string             $compareTo
     *
     * @return int
     * @throws InvalidTimezoneException
     */
    public function getDifferenceBetweenTimezones(\DateTimeInterface $dateTime, string $compareTo = 'UTC'): int
    {
        $timezone = $dateTime->getTimezone()->getName();
        $compareToDate = new \DateTime($dateTime->format('Y-m-d'), new \DateTimeZone($compareTo));

        if (!in_array($compareTo, $this->listSupportedTimezones(), true)) {
            throw new InvalidTimezoneException(sprintf('Unsupported %s timezone provided', $timezone));
        }

        $minutes = round(($dateTime->getOffset() - $compareToDate->getOffset()) / 60);

        return ($minutes % 15 === 0) ? $minutes : ceil($minutes / 15) * 15;
    }

    public function getNumberOfDaysInMonth(\DateTimeInterface $dateTime): int
    {
        return $dateTime->format('t');
    }

    public function getNumberOfDaysInFebruary(\DateTimeInterface $dateTime): int
    {
        return $dateTime->format('L') === '1' ? 29 : 28;
    }

    public function getMonthName(\DateTimeInterface $dateTime): string
    {
        return $dateTime->format('F');
    }

    public function listSupportedTimezones(): array
    {
        return \DateTimeZone::listIdentifiers();
    }
}
