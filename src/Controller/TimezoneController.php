<?php

namespace App\Controller;

use App\Form\DTO\DateTimezoneDTO;
use App\Form\Type\TimezoneType;
use App\Service\DateTimezoneService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimezoneController extends AbstractController
{
    /**
     * @var DateTimezoneService
     */
    private $dateTimezoneService;

    public function __construct(DateTimezoneService $dateTimezoneService)
    {
        $this->dateTimezoneService = $dateTimezoneService;
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \App\Exception\InvalidTimezoneException
     */
    public function index(Request $request): Response
    {
        $dateTimezoneDTO = new DateTimezoneDTO();
        $form = $this->createForm(TimezoneType::class, $dateTimezoneDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateTimezoneDTO = $form->getData();
            assert($dateTimezoneDTO instanceof DateTimezoneDTO);

            return $this->render(
                'timezone/output.html.twig',
                [
                    'input_timezone' => $dateTimezoneDTO->getTimezone()->getName(),
                    'offset' => $this->dateTimezoneService->getDifferenceBetweenTimezones($dateTimezoneDTO->getDate()->setTimezone($dateTimezoneDTO->getTimezone())),
                    'february_number_of_days' => $this->dateTimezoneService->getNumberOfDaysInFebruary($dateTimezoneDTO->getDate()),
                    'month_name' => $this->dateTimezoneService->getMonthName($dateTimezoneDTO->getDate()),
                    'month_days' => $this->dateTimezoneService->getNumberOfDaysInMonth($dateTimezoneDTO->getDate()),
                ]
            );
        }

        return $this->render(
            'timezone/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
