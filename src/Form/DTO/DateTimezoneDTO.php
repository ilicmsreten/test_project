<?php

namespace App\Form\DTO;

class DateTimezoneDTO
{
    /**
     * @var \DateTime|null
     */
    protected $date;

    /**
     * @var \DateTimeZone|null
     */
    protected $timezone;

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return \DateTimeZone|null
     */
    public function getTimezone(): ?\DateTimeZone
    {
        return $this->timezone;
    }

    /**
     * @param \DateTimeZone|null $timezone
     */
    public function setTimezone(?\DateTimeZone $timezone): void
    {
        $this->timezone = $timezone;
    }
}
