<?php

namespace App\Form\Type;

use App\Form\DataTransformer\DateStringDataTransformer;
use App\Form\DataTransformer\TimezoneStringDataTransformer;
use App\Form\DTO\DateTimezoneDTO;
use App\Service\DateTimezoneService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimezoneType extends AbstractType
{
    /**
     * @var DateTimezoneService
     */
    private $dateTimezoneService;

    /**
     * @var DateStringDataTransformer
     */
    private $dateStringDataTransformer;

    /**
     * @var TimezoneStringDataTransformer
     */
    private $timezoneStringDataTransformer;

    public function __construct(DateTimezoneService $dateTimezoneService, DateStringDataTransformer $dateStringDataTransformer, TimezoneStringDataTransformer $timezoneStringDataTransformer)
    {
        $this->dateTimezoneService = $dateTimezoneService;
        $this->dateStringDataTransformer = $dateStringDataTransformer;
        $this->timezoneStringDataTransformer = $timezoneStringDataTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', TextType::class)
            ->add(
                'timezone',
                ChoiceType::class,
                [
                    'choices' => array_combine($this->dateTimezoneService->listSupportedTimezones(), $this->dateTimezoneService->listSupportedTimezones()),
                ]
            )
            ->add('submit', SubmitType::class);

        $builder->get('date')->addModelTransformer($this->dateStringDataTransformer);
        $builder->get('timezone')->addModelTransformer($this->timezoneStringDataTransformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => DateTimezoneDTO::class,
            ]
        );
    }
}
