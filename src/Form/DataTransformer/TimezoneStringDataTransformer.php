<?php

namespace App\Form\DataTransformer;

use App\Exception\InvalidTimezoneException;
use App\Service\DateTimezoneService;
use Symfony\Component\Form\DataTransformerInterface;

class TimezoneStringDataTransformer implements DataTransformerInterface
{
    /**
     * @var DateTimezoneService
     */
    private $dateTimezoneService;

    public function __construct(DateTimezoneService $dateTimezoneService)
    {
        $this->dateTimezoneService = $dateTimezoneService;
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    public function transform($value): string
    {
        if (!$value instanceof \DateTimeZone) {
            return '';
        }

        return $value->getName();
    }

    /**
     * @param mixed $value
     *
     * @return \DateTimeZone
     * @throws InvalidTimezoneException
     */
    public function reverseTransform($value): \DateTimeZone
    {
        if (!in_array($value, $this->dateTimezoneService->listSupportedTimezones(), true)) {
            throw new InvalidTimezoneException(sprintf('Unsupported %s timezone provided', $value));
        }

        return new \DateTimeZone($value);
    }
}
