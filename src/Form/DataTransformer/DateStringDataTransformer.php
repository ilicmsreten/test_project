<?php

namespace App\Form\DataTransformer;

use App\Exception\InvalidDateValueException;
use Symfony\Component\Form\DataTransformerInterface;

class DateStringDataTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $value
     *
     * @return string
     */
    public function transform($value): string
    {
        if (!$value instanceof \DateTimeInterface) {
            return '';
        }

        return $value->format('Y-m-d');
    }

    /**
     * @param mixed $value
     *
     * @return \DateTimeInterface
     * @throws InvalidDateValueException
     */
    public function reverseTransform($value): \DateTimeInterface
    {
        $datetime = \DateTime::createFromFormat('Y-m-d', $value);

        if (!$datetime) {
            throw new InvalidDateValueException(sprintf('Unable to transform %s to DateTime', $value));
        }

        return $datetime;
    }

}
