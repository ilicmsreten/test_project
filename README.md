# Test project

## Note
There's one exception with your provided example values and real values.
PHP for the
```
   (new \DateTime('1853-01-30', new \DateTimeZone('America/Lower_Princes')))->getOffset();
```        
outputs ``-16547`` approx. 276min, so didn't have time to investigate what causing this behaviour and when Timezones are introduced, so I rounded those values to mod 15. 

## Run project using built in server
```
$ php -S 127.0.0.1:8010 public/index.php
```
## Run tests
```
$ php bin/phpunit
```
