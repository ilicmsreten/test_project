<?php

namespace App\Tests\Form\DataTransformer;

use App\Exception\InvalidTimezoneException;
use App\Form\DataTransformer\TimezoneStringDataTransformer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TimezoneStringDataTransformerTest  extends KernelTestCase
{
    public function testReverseTransformInvalidValueProvided(): void
    {
        $this->expectException(InvalidTimezoneException::class);
        $this->getTransformer()->reverseTransform('some invalid value');
    }

    public function testReverseTransform(): void
    {
        $this->assertInstanceOf(\DateTimeZone::class, $this->getTransformer()->reverseTransform('UTC'));
    }

    public function testTransformNullProvided(): void
    {
        $this->assertSame('', $this->getTransformer()->transform(null));
    }

    public function testTransform(): void
    {
        $this->assertSame('Asia/Tokyo', $this->getTransformer()->transform(new \DateTimeZone('Asia/Tokyo')));
    }

    private function getTransformer(): TimezoneStringDataTransformer
    {
        self::bootKernel();

        $transformer =  self::$container->get(TimezoneStringDataTransformer::class);

        assert($transformer instanceof TimezoneStringDataTransformer);

        return $transformer;
    }
}
