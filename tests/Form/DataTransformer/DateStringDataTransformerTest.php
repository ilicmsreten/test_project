<?php

namespace App\Tests\Form\DataTransformer;

use App\Exception\InvalidDateValueException;
use App\Form\DataTransformer\DateStringDataTransformer;
use PHPUnit\Framework\TestCase;

class DateStringDataTransformerTest extends TestCase
{
    public function testReverseTransformInvalidValueProvided(): void
    {
        $this->expectException(InvalidDateValueException::class);
        $this->getTransformer()->reverseTransform('some invalid value');
    }

    public function testReverseTransform(): void
    {
        $this->assertInstanceOf(\DateTimeInterface::class, $this->getTransformer()->reverseTransform('2011-01-01'));
    }

    public function testTransformNullProvided(): void
    {
        $this->assertSame('', $this->getTransformer()->transform(null));
    }

    public function testTransform(): void
    {
        $this->assertSame('2011-01-01', $this->getTransformer()->transform(new \DateTime('2011-01-01')));
    }

    private function getTransformer(): DateStringDataTransformer
    {
        return new DateStringDataTransformer();
    }
}
