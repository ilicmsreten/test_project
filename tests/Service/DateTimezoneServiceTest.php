<?php

namespace App\Tests\Service;

use App\Service\DateTimezoneService;
use PHPUnit\Framework\TestCase;

class DateTimezoneServiceTest extends TestCase
{
    /**
     * @param int $year
     * @param int $expected
     *
     * @dataProvider numberOfDaysInFebruaryDataProvider
     * @throws \Exception
     */
    public function testNumberOfDaysInFebruary(int $year, int $expected): void
    {
        $service = new DateTimezoneService();
        $this->assertSame($expected, $service->getNumberOfDaysInFebruary(new \DateTime(sprintf('%s-01-01', $year))));
    }

    /**
     * @return array|\int[][]
     */
    public function numberOfDaysInFebruaryDataProvider(): array
    {
        return [
            [
                'year' => 1650,
                'expected' => 28,
            ],
            [
                'year' => 1654,
                'expected' => 28,
            ],
            [
                'year' => 1878,
                'expected' => 28,
            ],
            [
                'year' => 1903,
                'expected' => 28,
            ],
            [
                'year' => 2000,
                'expected' => 29,
            ],
            [
                'year' => 2012,
                'expected' => 29,
            ],
            [
                'year' => 2021,
                'expected' => 28,
            ],
            [
                'year' => 2100,
                'expected' => 28,
            ],
        ];
    }

    /**
     * @param \DateTime $date
     * @param string    $expected
     *
     * @dataProvider getMonthNameDataProvider
     */
    public function testGetMonthName(\DateTime $date, string $expected): void
    {
        $service = new DateTimezoneService();
        $this->assertSame($expected, $service->getMonthName($date));
    }

    /**
     * @return array|\int[][]
     */
    public function getMonthNameDataProvider(): array
    {
        return [
            [
                'date' => new \DateTime('2012-01-07'),
                'expected' => 'January',
            ],
            [
                'date' => new \DateTime('2017-06-02'),
                'expected' => 'June',
            ],
            [
                'date' => new \DateTime('2000-12-07'),
                'expected' => 'December',
            ],
        ];
    }

    /**
     * @param \DateTime $date
     * @param int       $expected
     *
     * @dataProvider getNumberOfDaysInMonthDataProvider
     */
    public function testGetNumberOfDaysInMonth(\DateTime $date, int $expected): void
    {
        $service = new DateTimezoneService();
        $this->assertSame($expected, $service->getNumberOfDaysInMonth($date));
    }

    /**
     * @return array|\int[][]
     */
    public function getNumberOfDaysInMonthDataProvider(): array
    {
        return [
            'january' => [
                'date' => new \DateTime('2012-01-07'),
                'expected' => 31,
            ],
            'june' => [
                'date' => new \DateTime('2017-06-02'),
                'expected' => 30,
            ],
            'december' => [
                'date' => new \DateTime('2000-12-07'),
                'expected' => 31,
            ],
        ];
    }

    /**
     * @param \DateTimeInterface $dateTime
     * @param string             $compareTo
     * @param int                $expected
     *
     * @throws \App\Exception\InvalidTimezoneException
     * @dataProvider getDifferenceBetweenTimezonesDataProvider
     */
    public function testGetDifferenceBetweenTimezones(\DateTimeInterface $dateTime, string $compareTo, int $expected): void
    {
        $service = new DateTimezoneService();
        $this->assertSame($expected, $service->getDifferenceBetweenTimezones($dateTime, $compareTo));
    }

    /**
     * @return array|\int[][]
     */
    public function getDifferenceBetweenTimezonesDataProvider(): array
    {
        return [
            [
                'date' => new \DateTime('2019-07-10', new \DateTimeZone('Europe/London')),
                'compareTo' => 'UTC',
                'expected' => 60,
            ],
            [
                'date' => new \DateTime('2016-11-28', new \DateTimeZone('Asia/Tokyo')),
                'compareTo' => 'UTC',
                'expected' => 540,
            ],
            [
                'date' => new \DateTime('1853-01-30', new \DateTimeZone('America/Lower_Princes')),
                'compareTo' => 'UTC',
                'expected' => -270,
            ],
            [
                'date' => new \DateTime('2012-01-30', new \DateTimeZone('America/Lower_Princes')),
                'compareTo' => 'UTC',
                'expected' => -240,
            ],
            [
                'date' => new \DateTime('2012-01-30', new \DateTimeZone('Asia/Tokyo')),
                'compareTo' => 'Europe/Madrid',
                'expected' => 480,
            ],
        ];
    }
}
